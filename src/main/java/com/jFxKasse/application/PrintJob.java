package com.jFxKasse.application;

import java.util.ArrayList;

import com.jFxKasse.controller.DBController;
import com.jFxKasse.controller.PrinterController;
import com.jFxKasse.controller.XMLController;

import com.jFxKasse.datatypes.PrintDataSimple;
import com.jFxKasse.datatypes.PrintDataSplitted;

import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

public class PrintJob
{

	private TimeDate timedate = new TimeDate();

	public void printJob(int jobID, XMLController xmlc, DBController dbc,
			PrinterController pc)
	{

		if ((xmlc.getPrintername().equals("Drucker auswählen")
				|| xmlc.getPrintername() == null)) {
			// no printer selected
			System.out.println("Kein Drucker eingestellt!!!");

			// creates a dialog
			Dialog<Pair<String, String>> dialog = new Dialog<>();
			dialog.setTitle("Kein Drucker");
			dialog.setHeaderText("Es ist kein Drucker einestellt.\n"
					+ "In den Einstellungen einen Drucker auswählen.");

			dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(20, 150, 10, 10));

			grid.add(new Label("Tipp:\n"
					+ "Es kann ein virtueller Drucker installiert werden: \n\n"
					+ "www.cups-pdf.de"), 0, 0);

			dialog.getDialogPane().setContent(grid);
			dialog.setResizable(true);
			dialog.showAndWait();

		} else {
			// printer selected
			pc.selectPrinter(xmlc.getPrintername());
			/* Single bill or splitted */
			if (xmlc.getCategorySplitted()) {
				// split the bills

				PrintDataSplitted pdsplitted = new PrintDataSplitted(
						xmlc.getLinebreak(), xmlc.getOffsetHeader(),
						xmlc.getOffsetFooter(),
						timedate.getSystemTime() + " " + timedate.getSystemDate(),
						xmlc.getHeader(), xmlc.getFooter());

				pdsplitted.setData(Integer.toString(jobID), dbc.getTime_Job(jobID),
						dbc.getQuantity_Job(jobID), dbc.getName_Job(jobID),
						dbc.getValue_Job(jobID), dbc.getCategory_Job(jobID),
						dbc.getJobValue_Job(jobID));

				System.out.println("Printing job ...");

				ArrayList<String> printString = pdsplitted.getPrintStrings();

				for (int i = 0; i < printString.size(); i++) {
					pc.printString(printString.get(i));
				}

			} else {
				// one single bills
				PrintDataSimple pds = new PrintDataSimple(xmlc.getLinebreak(),
						xmlc.getOffsetHeader(), xmlc.getOffsetFooter(),
						timedate.getSystemTime() + " " + timedate.getSystemDate(),
						xmlc.getHeader(), xmlc.getFooter());

				pds.setData(Integer.toString(jobID), dbc.getTime_Job(jobID),
						dbc.getQuantity_Job(jobID), dbc.getName_Job(jobID),
						dbc.getValue_Job(jobID), dbc.getCategory_Job(jobID),
						dbc.getJobValue_Job(jobID));
				System.out.println("Printing job ...");
				pc.printString(pds.getPrintString());
			}
		}

	}

}
