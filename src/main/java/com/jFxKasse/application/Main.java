package com.jFxKasse.application;

import javafx.application.Application;
import javafx.event.EventHandler;

import java.io.File;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.util.Duration;
import com.jFxKasse.controller.MainWindowController;
import com.jFxKasse.controller.PrinterController;
import com.jFxKasse.controller.XMLController;
import com.jFxKasse.controller.DBController;
import com.jFxKasse.controller.KeyController;

import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

public class Main extends Application
{
	// path to config.xml and the database
	public static String filepath;

	private static String osName = System.getProperty("os.name");

	private static String userHome = System.getProperty("user.home");

	private MainWindowController mwc;

	private XMLController xmlc = new XMLController(filepath);

	private DBController dbc = new DBController(filepath);

	private PrinterController pc = new PrinterController();
	
	private KeyController kc;

	private Stage primaryStage;

	@Override
	public void start(Stage primaryStage)
	{
		this.primaryStage = primaryStage;
		System.out.println("\nstarting jFxKasse\n");
		mainWindow();
	}

	private void mainWindow()
	{
		try {
			FXMLLoader loader = new FXMLLoader(
					getClass().getResource("/fxml/MainWindow.fxml"));

			AnchorPane pane = loader.load();
			primaryStage.setTitle("jFxKasse"); // Title of window

			mwc = loader.getController(); // set the mwc as the JavaFx
													// MainWindowController
			pc.searchPrinters(); // search for available printers
			mwc.setMain(this, dbc, xmlc, pc); // set the created instances to the
															// mwc

			firstStart(); // test if this is the first run

			Scene scene = new Scene(pane);
			scene.getStylesheets().add(
					Main.class.getResource("/css/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show(); // shows the stage

			//attach the KeyController
			kc = new KeyController(scene, mwc);
			
			Timeline timeline = new Timeline(
					new KeyFrame(Duration.seconds(1), ev -> {
						mwc.updateTimeLabel(); // update time label on UI
					}));
			timeline.setCycleCount(Animation.INDEFINITE);
			timeline.play();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		if (osName.contains("Windows")) {
			System.out.println("FCK Windows");
			filepath = userHome + "/Documents/jFxKasse/";
		} else {
			filepath = userHome + "/jFxKasse/";
		}
		launch(args);
	}

	/**
	 * Checks if the config.xml is preset.
	 */
	private void firstStart() throws Exception
	{
		if (xmlc.loadSettings()) {
			// config.xml found, app starting normal
			System.out.println("XML found!");
			mwc.initUI(); // Starting the UI elements
			mwc.setDBLabel(); // Set database labels
			dbc.setDbname(xmlc.getDatabaseName()); // handover database name
			dbc.connectDatabase(); // estabishing DB conection
			mwc.fillTablePositionen(); // fill TreeTable 'Positionen'
			mwc.fillCategory();
			mwc.fillPrinterSettings();
			mwc.fillTableJobs();
			mwc.loadGridButtons();
			mwc.getSelectedCat(); // Load DB entries in Chois Box
			mwc.createNewJob();
		} else {
			// config.xml NOT found, first start of app
			System.out.println("no XML found!");
			xmlc.initXML(); // set default values
			mwc.blockUI(true); // disable UI elements that need DB
			mwc.blockUnlock();
			File dir = new File(filepath);
			dir.mkdir(); // Create new Subfolder
		}
	}
}
