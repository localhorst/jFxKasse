package com.jFxKasse.application;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeDate
{
	public String getSystemTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");
		Date date = new Date();
		String time = dateFormat.format(date);
		return time;
	}

	public String getSystemDate()
	{
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		String dateStr = dateFormat.format(date);
		return dateStr;
	}
}
