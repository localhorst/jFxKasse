package com.jFxKasse.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import com.jFxKasse.datatypes.tableDataJob;
import com.jFxKasse.datatypes.tableDataPositionen;
import java.io.File;

public class DBController
{
	private Connection connection;

	private String DB_PATH;

	private String dbname;

	public void main()
	{
		try {
			connection = DriverManager
					.getConnection("jdbc:sqlite:" + DB_PATH + dbname + ".db");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public DBController(String path)
	{
		this.DB_PATH = path;
	}

	public void setDbname(String dbname)
	{
		this.dbname = dbname;
	}

	public void connectDatabase()
	{ // connect to database
		System.out.println("Connecting... DB name: " + dbname);
		try {
			if (connection != null)
				return;
			connection = DriverManager
					.getConnection("jdbc:sqlite:" + DB_PATH + dbname + ".db");
			if (!connection.isClosed())
				System.out.println("DB connection established");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run()
			{
				try {
					if (!connection.isClosed() && connection != null) {
						connection.close();
						if (connection.isClosed())
							System.out.println();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public boolean existDB(String pPfad)
	{ // does the DB exists?
		File varTmpDir = new File(pPfad);
		if (!varTmpDir.exists()) {
			return false;
		} else {
			return true;
		}
	}

	public String getCategoryNameFromPositionen(int pID)
	{
		int catInPos = 0;
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT posid, cat FROM positionen "
					+ "WHERE posid = " + pID + ";");
			catInPos = rs.getInt("cat");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();

		}
		if (catInPos == 6) {
			return "Standard";
		}
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT catid, catname FROM category "
					+ "WHERE catid = " + catInPos + ";");
			return rs.getString("catname");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "keine Kategorie";
		}

	}

	// table Position section //
	public void createTablePositionen()
	{ // create table position
		System.out.println("Creating table Positionen");
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DROP TABLE IF EXISTS positionen;");
			stmt.executeUpdate(
					"CREATE TABLE positionen (posid, name, value, cat, color);");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}

		// create 25 demo/default data entries
		for (int i = 0; i < 25; i++) {
			fillPositionen_Positionen(i + 1, "Noch frei", (float) 0.00, 6,
					"#ad0000");
		}
	}

	public void fillPositionen_Positionen(int pID, String pName, float pValue,
			int pCat, String pColor)
	{ // create new data in table
		System.out.println("Creating new positionen entry");
		try {
			PreparedStatement ps = connection.prepareStatement(
					"INSERT INTO positionen VALUES (?, ?, ?, ?, ?);");
			ps.setInt(1, pID); // primary
			ps.setString(2, pName);
			ps.setFloat(3, pValue);
			ps.setInt(4, pCat);
			ps.setString(5, pColor);

			ps.addBatch();
			connection.setAutoCommit(false);
			ps.executeBatch(); // SQL execution
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	public String getName_Positionen(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT posid, name FROM positionen WHERE posid = " + pID + ";");
			return rs.getString("name");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "Error 404";
		}
	}

	public String getValue_Positionen(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT posid, value FROM positionen WHERE posid = " + pID
							+ ";");
			return rs.getString("value");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "Error 404";
		}
	}

	public int getCat_Positionen(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT catid, cat FROM positionen WHERE catid = " + pID + ";");
			System.out.println("getCarTet: " + rs.getInt("cat"));
			return rs.getInt("cat");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return 0;
		}
	}

	public String getColor_Positionen(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT posid, color FROM positionen WHERE posid = " + pID
							+ ";");
			return rs.getString("color");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "Error 404";
		}
	}

	public void setName_Positionen(int pID, String pName)
	{
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("UPDATE positionen SET name = '" + pName
					+ "'WHERE posid =" + pID + ";");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	public void setValue_Positionen(int pID, String pValue)
	{
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("UPDATE positionen SET value = '" + pValue
					+ "'WHERE posid =" + pID + ";");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	public void setCat_Positionen(int pID, int pCat)
	{
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("UPDATE positionen SET cat = '" + pCat
					+ "'WHERE posid =" + pID + ";");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	public void setColor_Positionen(int pID, String pColor)
	{
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("UPDATE positionen SET color = '" + pColor
					+ "'WHERE posid =" + pID + ";");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	public ArrayList<tableDataPositionen> ladeTabellePositionen()
	{
		ArrayList<tableDataPositionen> daten = new ArrayList<>();
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM positionen;");
			while (rs.next()) {
				try {
					daten.add(new tableDataPositionen(rs.getInt("posid"),
							rs.getString("name"), rs.getString("value"),
							rs.getString("cat"), rs.getString("color")));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
		return daten;
	}

	public void ausgebenSysoPositionen()
	{
		System.out.println("Print positionen");
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM positionen;");
			while (rs.next()) {
				System.out.println("posid = " + rs.getString("posid"));
				System.out.println("name = " + rs.getString("name"));
				System.out.println("vale = " + rs.getString("value"));
				System.out.println("color = " + rs.getString("color"));
				System.out.println(" ");
			}

		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	// table Category section //
	public void createTableCategory()
	{ // create table position
		System.out.println("Erstelle Tabelle Kategorie");
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DROP TABLE IF EXISTS category;");
			stmt.executeUpdate("CREATE TABLE category (catid, catname);");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}

		fillCategory_Category(1, "Essen");
		fillCategory_Category(2, "alkoholische Getränke");
		fillCategory_Category(3, "alkoholfreie Getränke");
		fillCategory_Category(4, "Kuchen");
		fillCategory_Category(5, "Standard");

	}

	public void setName_Category(int pID, String pName)
	{
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("UPDATE category SET catname = '" + pName
					+ "'WHERE catid =" + pID + ";");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	public void fillCategory_Category(int pID, String pName)
	{

		System.out.println("Erstelle neuen Kategorie Eintrag");
		try {
			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO category VALUES (?, ?);");
			ps.setInt(1, pID); // primary
			ps.setString(2, pName);
			ps.addBatch();
			connection.setAutoCommit(false);
			ps.executeBatch(); // SQL execution
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}

	}

	public String getName_Category(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT catid, catname FROM category WHERE catid = " + pID
							+ ";");
			return rs.getString("catname");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "Error 404";
		}
	}

	// table Jobs section //
	public void erstelleTabelleJobs()
	{ // create table jobs
		System.out.println("Creating table Jobs");
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DROP TABLE IF EXISTS jobs;");
			stmt.executeUpdate(
					"CREATE TABLE jobs (jobid, time, positionen_quantity, positionen_name, positionen_value, positionen_cat, state, jobvalue);");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	public int getLatestJobNumber_Job()
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid from jobs ORDER BY jobid DESC LIMIT 1;");
			return rs.getInt("jobid");
		} catch (SQLException e) {
			// System.err.println("Couldn't handle DB-Query");
			// e.printStackTrace();

		}
		return 0;
	}

	public String getTime_Job(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid, time FROM jobs WHERE jobid = " + pID + ";");
			return rs.getString("time");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "404";
		}
	}

	public String getQuantity_Job(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid, positionen_quantity FROM jobs WHERE jobid = "
							+ pID + ";");
			return rs.getString("positionen_quantity");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "404";
		}
	}

	public String getName_Job(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid, positionen_name FROM jobs WHERE jobid = " + pID
							+ ";");
			return rs.getString("positionen_name");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "404";
		}
	}

	public String getValue_Job(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid, positionen_value FROM jobs WHERE jobid = " + pID
							+ ";");
			return rs.getString("positionen_value");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "404";
		}
	}

	public String getCategory_Job(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid, positionen_cat FROM jobs WHERE jobid = " + pID
							+ ";");
			return rs.getString("positionen_cat");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "404";
		}
	}

	public String getState_Job(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid, state FROM jobs WHERE jobid = " + pID + ";");
			return rs.getString("state");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "404";
		}
	}

	public String getJobValue_Job(int pID)
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid, jobvalue FROM jobs WHERE jobid = " + pID + ";");
			return rs.getString("jobvalue");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "404";
		}
	}

	public String getAllJobValue_Job()
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT state, jobvalue, SUM(jobvalue) AS ALLVALUE FROM jobs WHERE state = "
							+ '"' + "verbucht" + '"' + ";");
			return rs.getString("ALLVALUE");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "0";
		}
	}

	public String getJobCount()
	{
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT count(*) AS JOBCOUNT FROM jobs WHERE state = " + '"'
							+ "verbucht" + '"' + ";");
			return rs.getString("JOBCOUNT");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
			return "0";
		}
	}

	public void setStatus_Jobs(int pID, String pStatus)
	{
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("UPDATE jobs SET state = '" + pStatus
					+ "'WHERE jobid =" + pID + ";");
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}

	public ArrayList<tableDataJob> loadTableJobs_Job()
	{

		ArrayList<tableDataJob> tmp = new ArrayList<tableDataJob>();
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT jobid, time, positionen_quantity, positionen_name, positionen_value, positionen_cat, state, jobvalue FROM jobs;");
			while (rs.next()) {
				try {

					String tablePosition = rs.getString("positionen_name");

					tableDataJob data = new tableDataJob(rs.getInt("jobid"),
							rs.getString("time"), tablePosition, rs.getString("state"),
							rs.getString("jobvalue"));

					tmp.add(data);
				} catch (SQLException e) {
					System.err.println("Couldn't handle DB-Query");
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
		return tmp;
	}

	public void fillJobs_Jobs(int pID, String pTime, String pPositionen_quantity,
			String pPositionen_name, String pPositionen_value,
			String pPositionen_cat, String pState, String pJobvalue)
	{
		System.out.println("Create new Job Entry");
		try {
			PreparedStatement ps = connection.prepareStatement(
					"INSERT INTO jobs VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
			ps.setInt(1, pID); // primary
			ps.setString(2, pTime);
			ps.setString(3, pPositionen_quantity);
			ps.setString(4, pPositionen_name);
			ps.setString(5, pPositionen_value);
			ps.setString(6, pPositionen_cat);
			ps.setString(7, pState);
			ps.setString(8, pJobvalue);
			ps.addBatch();
			connection.setAutoCommit(false);
			ps.executeBatch(); // SQL execution
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}
	}
}
