package com.jFxKasse.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class XMLController
{
	private String databaseName = null;

	private String printername = null;

	private int linebreak;

	private int offsetHeader;

	private int offsetFooter;

	private String header = null;

	private String footer = null;

	private boolean categorySplitted;

	private String filePath = null;

	private Properties props = null;

	public XMLController(String filePath)
	{
		this.filePath = filePath + "config.xml";
		props = new Properties();
	}

	public void saveSettings() throws Exception
	{ // Save settings to config.xml

		System.out.println("Saving XML");

		OutputStream outputStream;

		String linebreak = Integer.toString(this.linebreak);
		String offsetHeader = Integer.toString(this.offsetHeader);
		String offsetFooter = Integer.toString(this.offsetFooter);

		String categorySplitted = null;

		if (this.categorySplitted) {
			categorySplitted = "true";
		} else {
			categorySplitted = "false";
		}

		try {
			props.setProperty("databasename", this.databaseName);
			props.setProperty("printername", this.printername);
			props.setProperty("linebreak", linebreak);
			props.setProperty("offsetHeader", offsetHeader);
			props.setProperty("offsetFooter", offsetFooter);
			props.setProperty("header", this.header);
			props.setProperty("footer", this.footer);
			props.setProperty("categorySplitted", categorySplitted);

			outputStream = new FileOutputStream(filePath);
			props.storeToXML(outputStream, "jFxKasse settings");
			outputStream.close();
		} catch (IOException e) {
		}
	}

	public boolean loadSettings() throws Exception
	{ // reads the settings from config.xml
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(filePath);
			props.loadFromXML(inputStream);
			this.databaseName = (props.getProperty("databasename"));
			this.printername = (props.getProperty("printername"));

			try {
				this.linebreak = Integer.valueOf(props.getProperty("linebreak"));
			} catch (Exception e) {
				this.linebreak = 28;
			}

			try {
				this.offsetHeader = Integer
						.valueOf(props.getProperty("offsetHeader"));
			} catch (Exception e) {
				this.offsetHeader = 1;
			}

			try {
				this.offsetFooter = Integer
						.valueOf(props.getProperty("offsetFooter"));
			} catch (Exception e) {
				this.offsetFooter = 2;
			}

			this.header = (props.getProperty("header"));
			this.footer = (props.getProperty("footer"));

			try {
				if (props.getProperty("categorySplitted").equals("true")) {
					this.categorySplitted = true;
				} else {
					this.categorySplitted = false;
				}
			} catch (Exception e) {
				this.categorySplitted = false;
			}

			inputStream.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void initXML()
	{
		this.printername = "Drucker auswählen";

		this.offsetHeader = 1;
		this.offsetFooter = 2;

		this.linebreak = 28;

		this.categorySplitted = false;

		this.header = "XYZ GmbH";
		this.footer = "Vielen Dank für den Einkauf";
	}

	public String getDatabaseName()
	{
		return databaseName;
	}

	public void setDatabaseName(String databaseName)
	{
		this.databaseName = databaseName;
	}

	public String getPrintername()
	{
		return printername;
	}

	public void setPrintername(String printername)
	{
		this.printername = printername;
	}

	public int getLinebreak()
	{
		return linebreak;
	}

	public void setLinebreak(int linebreak)
	{
		this.linebreak = linebreak;
	}

	public int getOffsetHeader()
	{
		return offsetHeader;
	}

	public void setOffsetHeader(int offsetHeader)
	{
		this.offsetHeader = offsetHeader;
	}

	public int getOffsetFooter()
	{
		return offsetFooter;
	}

	public void setOffsetFooter(int offsetFooter)
	{
		this.offsetFooter = offsetFooter;
	}

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

	public String getFooter()
	{
		return footer;
	}

	public void setFooter(String footer)
	{
		this.footer = footer;
	}

	public boolean getCategorySplitted()
	{
		return categorySplitted;
	}

	public void setCategorySplitted(boolean categorySplitted)
	{
		this.categorySplitted = categorySplitted;
	}

}
