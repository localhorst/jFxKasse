/**
 *  some parts are from http://www.mets-blog.com/java-pos-thermal-printer-example/
 */

package com.jFxKasse.controller;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.nio.charset.StandardCharsets;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

public class PrinterController implements Printable
{
	// All available Printers on this system
	private PrintService[] printService;

	// selected printer
	private PrintService selectedPrinter;

	private DocFlavor flavor;

	public PrinterController()
	{
		flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	}

	/**
	 * @return A String array with all available printers
	 */
	public String[] getAvailablePrinters()
	{
		int printerSize = PrinterJob.lookupPrintServices().length;
		String printers[] = new String[printerSize];

		for (int i = 0; i < printers.length; i++) {
			printers[i] = this.printService[i].getName();
		}
		return printers;
	}

	/**
	 * searchs connected printers on the system
	 */
	public void searchPrinters()
	{
		PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
		this.printService = PrintServiceLookup.lookupPrintServices(flavor, pras);
		String printers[] = getAvailablePrinters();
		System.out.println("Printers found:");
		System.out.println("++++++++++++++++++++");
		for (int i = 0; i < printers.length; i++) {
			System.out.println(printers[i]);
		}
		System.out.println("++++++++++++++++++++");
	}

	/**
	 * Selects the printer via its name
	 * @param printerName
	 */
	public void selectPrinter(String printerName)
	{
		String printers[] = getAvailablePrinters();

		for (int i = 0; i < printers.length; i++) {
			if (printerName.equals(printers[i])) {
				selectedPrinter = printService[i];
				System.out.println("Printer: " + printers[i] + " selected");
				return;
			}
		}
	}

	/**
	 * 
	 * @param input data as String
	 */
	public void printString(String text)
	{
		PrintService service = selectedPrinter;
		DocPrintJob job = service.createPrintJob();

		try {
			byte[] bytes;
			bytes = text.getBytes(StandardCharsets.UTF_8);
			Doc doc = new SimpleDoc(bytes, flavor, null);
			job.print(doc, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int print(Graphics g, PageFormat pf, int page) throws PrinterException
	{
		if (page > 0) { /* We have only one page, and 'page' is zero-based */
			return NO_SUCH_PAGE;
		}

		/*
		 * User (0,0) is typically outside the imageable area, so we must
		 * translate by the X and Y values in the PageFormat to avoid clipping
		 */
		Graphics2D g2d = (Graphics2D) g;
		g2d.translate(pf.getImageableX(), pf.getImageableY());
		/* Now we perform our rendering */
		return PAGE_EXISTS;
	}

	public void printBytes(byte[] bytes)
	{
		PrintService service = selectedPrinter;
		DocPrintJob job = service.createPrintJob();

		try {
			Doc doc = new SimpleDoc(bytes, flavor, null);
			job.print(doc, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cutPaper()
	{
		byte[] cutP = new byte[] { 0x1d, 'V', 1 };
		printBytes(cutP);
	}
}
