package com.jFxKasse.controller;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.ChoiceBox;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import com.jFxKasse.application.Main;
import com.jFxKasse.application.PrintJob;
import com.jFxKasse.application.TimeDate;
import com.jFxKasse.datatypes.Job;
import com.jFxKasse.datatypes.tableDataCurrentOrder;
import com.jFxKasse.datatypes.tableDataJob;
import com.jFxKasse.datatypes.tableDataPositionen;

public class MainWindowController
{
	@FXML
	private AnchorPane mainAnchorpane;

	@FXML
	private AnchorPane paneDB;

	@FXML
	private Tab tapNewJob;

	@FXML
	private Tab tapJobs;

	@FXML
	private Tab tapPosEdit;

	@FXML
	private Tab tapSettings;

	@FXML
	private TreeTableView<tableDataCurrentOrder> tableCurrentOrder;

	@FXML
	private TreeTableColumn<tableDataCurrentOrder, Integer> columnQuantity;

	@FXML
	private TreeTableColumn<tableDataCurrentOrder, String> columnPosition;

	@FXML
	private TreeTableColumn<tableDataCurrentOrder, Integer> idSpalte01 = new TreeTableColumn<>(
			"tableCurrentOrder");

	@FXML
	private TreeTableView<tableDataJob> jobsTreeTable;

	@FXML
	private TreeTableColumn<tableDataJob, String> columnJobValue;

	@FXML
	private TreeTableColumn<tableDataJob, String> columnState;

	@FXML
	private TreeTableColumn<tableDataJob, String> columnPositions;

	@FXML
	private TreeTableColumn<tableDataJob, String> columnTime;

	@FXML
	private TreeTableColumn<tableDataJob, Integer> columnJobNumber;

	@FXML
	private TreeTableColumn<tableDataJob, Integer> idSpalte02 = new TreeTableColumn<>(
			"jobsTreeTable");

	@FXML
	private TreeTableView<tableDataPositionen> entryTreeTable;

	@FXML
	private TreeTableColumn<tableDataPositionen, String> columnColor;

	@FXML
	private TreeTableColumn<tableDataPositionen, String> columnCat;

	@FXML
	private TreeTableColumn<tableDataPositionen, String> columnPrize;

	@FXML
	private TreeTableColumn<tableDataPositionen, String> columnPositionsEdit;

	@FXML
	private TreeTableColumn<tableDataPositionen, Integer> columnPosnumber;

	@FXML
	private ChoiceBox<String> colorChoise;

	@FXML
	private ChoiceBox<String> catChoise;

	@FXML
	private ChoiceBox<String> printerChoise;

	@FXML
	private Spinner<Integer> linesSpinner;

	@FXML
	private Spinner<Integer> offsetHeaderSpinner;

	@FXML
	private Spinner<Integer> offsetFooterSpinner;

	@FXML
	private Button ueberbtn;

	@FXML
	private Button gridButton01;

	@FXML
	private Button gridButton02;

	@FXML
	private Button gridButton03;

	@FXML
	private Button gridButton04;

	@FXML
	private Button gridButton05;

	@FXML
	private Button gridButton06;

	@FXML
	private Button gridButton07;

	@FXML
	private Button gridButton08;

	@FXML
	private Button gridButton09;

	@FXML
	private Button gridButton10;

	@FXML
	private Button gridButton11;

	@FXML
	private Button gridButton12;

	@FXML
	private Button gridButton13;

	@FXML
	private Button gridButton14;

	@FXML
	private Button gridButton15;

	@FXML
	private Button gridButton16;

	@FXML
	private Button gridButton17;

	@FXML
	private Button gridButton18;

	@FXML
	private Button gridButton19;

	@FXML
	private Button gridButton20;

	@FXML
	private Button gridButton21;

	@FXML
	private Button gridButton22;

	@FXML
	private Button gridButton23;

	@FXML
	private Button gridButton24;

	@FXML
	private Button gridButton25;

	@FXML
	public Button btnSavePrinter;

	@FXML
	public Button btnDeleteSelectedPosition;

	@FXML
	public Button btnPrintBill;

	@FXML
	public Button btnLock;

	@FXML
	public Button btnReprintJob;

	@FXML
	public Button btnCancelJob;

	@FXML
	public Button btnCalcStats;

	@FXML
	public Button btnSaveEntry;

	@FXML
	public Button btnClearEntry;

	@FXML
	public Button btnCreateNewDatabase;

	@FXML
	private Button btnOpenFolder;

	@FXML
	private Label labelCat01;

	@FXML
	private Label labelCat02;

	@FXML
	private Label labelCat05;

	@FXML
	private Label labelCat04;

	@FXML
	private Label labelCat03;

	@FXML
	private JFXTextField tftfooter;

	@FXML
	private JFXTextField tftheader;

	@FXML
	private JFXTextField tftKat01;

	@FXML
	private JFXTextField tftKat02;

	@FXML
	private JFXTextField tftKat03;

	@FXML
	private JFXTextField tftKat04;

	@FXML
	private JFXTextField tftKat05;

	@FXML
	public Button btnSaveCat;

	@FXML
	private Label labelAllPrize;

	@FXML
	private Label labelJobCounter;

	@FXML
	private Label labelTime;

	@FXML
	private Label labelJobCount;

	@FXML
	private Label labelAvgJob;

	@FXML
	private Label labelAllValue;

	@FXML
	private Label labelSelectCat;

	@FXML
	private Label labelNewPosition;

	@FXML
	private Label labelNewValue;

	@FXML
	private Label labelNewColor;

	@FXML
	private Label labelDBStatus;

	@FXML
	private Label labelDBName;

	@FXML
	private TitledPane titlePaneStats;

	@FXML
	private TitledPane titlePaneCat;

	@FXML
	private TitledPane titledPaneEntry;

	@FXML
	private TitledPane titlePanePrint;

	@FXML
	private TextField tftNewPosition;

	@FXML
	private TextField tftNewValue;

	@FXML
	private TextField tftNewDBName;

	@FXML
	private JFXToggleButton switchSeparate;

	@SuppressWarnings("unused")
	private Main main;

	private DBController dbc;

	private XMLController xmlc;

	private PrinterController pc;

	private TimeDate timedate = new TimeDate();

	private PrintJob pj = new PrintJob();

	private int idPositionen = 0;

	private String selectedColorName;

	private String selectedCatName;

	private boolean lockState = false;

	private boolean isPrintBtnDisabled = true;

	private Job currentJob = null;

	private String currentOrderPosName;

	private int selectedJobId;

	@FXML
	TreeItem<tableDataCurrentOrder> rootCurrentJob = new TreeItem<>(
			new tableDataCurrentOrder("0", 0));

	@FXML
	TreeItem<tableDataJob> rootJobs = new TreeItem<>(
			new tableDataJob(0, "0", "0", "0", "0"));

	@FXML
	TreeItem<tableDataPositionen> rootPositionen = new TreeItem<>(
			new tableDataPositionen(0, "0", "0", "0", "0"));

	@FXML
	public void ueberbtnAction(ActionEvent event)
	{ // opens the 'Über' dialog

		// creates a dialog
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle("Über jFxKasse");
		dialog.setHeaderText("Informationen und Lizenzen - Version 0.3.2");

		dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		grid.add(new Label(
				"Einfaches Kassensystem für kleine bis mittel große Veranstaltungen mit Bon-Drucker\n"
						+ "\nUnter Lizenz GPL-3.0 abrufbar auf https://git.mosad.xyz/localhorst/jFxKasse\n"
						+ "\nDatenbank: sqlite.org - Public Domain"
						+ " \nUI Design01: eclipse.org/efxclipse/install.html - Eclipse Public License 1.0"
						+ " \nUI Design02: http://www.jfoenix.com/ -  Apache License 2.0"
						+ " \nUI - Datenbank Integration: basierend auf Project-HomeFlix - github.com/Seil0/Project-HomeFlix - GPLv3 \n"
						+ " \nMaintainer: hendrik.schutter@coptersicht.de"
						+ " \n(c) 2018 Hendrik Schutter"),
				0, 0);

		dialog.getDialogPane().setContent(grid);
		dialog.setResizable(true);
		dialog.showAndWait();
	}

	@FXML
	public void btnOpenFolderAction(ActionEvent event) throws IOException
	{

		if (System.getProperty("os.name").contains("Windows")) {
			System.out.println("FCK Windows");
			File file = new File(Main.filepath);
			Desktop desktop = Desktop.getDesktop();
			desktop.open(file);
		} else {
			Runtime.getRuntime().exec("xdg-open " + Main.filepath);
		}
	}

	@FXML
	public void btnCreateNewDatabaseAction(ActionEvent event) throws Exception
	{

		if (!(tftNewDBName.getText().equals(""))) {

			xmlc.setDatabaseName(tftNewDBName.getText());

			dbc.setDbname(xmlc.getDatabaseName());
			dbc.connectDatabase(); // establish DB connection
			dbc.createTablePositionen(); // Create new table
			dbc.erstelleTabelleJobs(); // Create new table
			dbc.createTableCategory(); // Create new table
			try {
				// saveSettings(getDatabaseName());
				xmlc.saveSettings();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			setDBLabel(); // Set new databese labels
			blockUI(false); // unlock UI elements that need DB
			fillTablePositionen(); // fill TreeTable 'Positionen'
			fillCategory();
			fillPrinterSettings();
			initUI(); // Starting the UI elements
			getSelectedCat();
			createNewJob();

			btnLock.setDisable(false);
		}
	}

	@FXML
	public void btnSaveEntryAction(ActionEvent event)
	{
		dbc.setName_Positionen(idPositionen, tftNewPosition.getText());
		dbc.setValue_Positionen(idPositionen, tftNewValue.getText());
		dbc.setColor_Positionen(idPositionen, getColorCodes(selectedColorName));

		dbc.setCat_Positionen(idPositionen, getSelectedCat());

		fillTablePositionen(); // fill TreeTable 'Positionen'
		loadGridButtons();

	}

	@FXML
	public void btnClearEntryAction(ActionEvent event)
	{
		// set default values
		dbc.setName_Positionen(idPositionen, "Noch frei");
		dbc.setValue_Positionen(idPositionen, "0.00");
		dbc.setColor_Positionen(idPositionen, "#FAF0E6");
		dbc.setCat_Positionen(idPositionen, 6);

		fillTablePositionen(); // fill TreeTable 'Positionen'
	}

	@FXML
	public void btnCalcStatsAction(ActionEvent event)
	{
		int allActiveJobs = Integer.valueOf(dbc.getJobCount());

		if (dbc.getAllJobValue_Job() != null) {

			// summed up values of all jobs that are active
			float allValue = Float.valueOf(dbc.getAllJobValue_Job());

			// count of all jobs that are active

			float avgJobValue = ((float) allValue / allActiveJobs);

			setJobStatLabel(allActiveJobs, avgJobValue, allValue);
		} else {
			setJobStatLabel(allActiveJobs, 0, 0);
		}
		titlePaneStats.setVisible(true);
	}

	@FXML
	public void btnSavePrinterAction(ActionEvent event)
	{
		System.out.println("btnSavePrinterAction");

		xmlc.setPrintername(
				printerChoise.getSelectionModel().getSelectedItem().toString());
		xmlc.setLinebreak(linesSpinner.getValue());
		xmlc.setOffsetHeader(offsetHeaderSpinner.getValue());
		xmlc.setOffsetFooter(offsetFooterSpinner.getValue());
		xmlc.setHeader(tftheader.getText());
		xmlc.setFooter(tftfooter.getText());
		xmlc.setCategorySplitted(switchSeparate.isSelected());

		try {
			xmlc.saveSettings();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		fillPrinterSettings();
	}

	@FXML
	public void btnCancelJobAction(ActionEvent event)
	{
		dbc.setStatus_Jobs(selectedJobId + 1, "storniert");
		fillTableJobs();
		btnCancelJob.setDisable(true);
		btnReprintJob.setDisable(true);
	}

	@FXML
	public void btnLockAction(ActionEvent event)
	{
		lockState = !lockState;
		blockUI(lockState);

		if (lockState) {
			btnLock.setText("Kasse entsperren");
		} else {
			btnLock.setText("Kasse sperren");
		}
	}

	@FXML
	public void btnSaveCatAction(ActionEvent event)
	{
		dbc.setName_Category(1, tftKat01.getText());
		dbc.setName_Category(2, tftKat02.getText());
		dbc.setName_Category(3, tftKat03.getText());
		dbc.setName_Category(4, tftKat04.getText());
		dbc.setName_Category(5, tftKat05.getText());

		fillCategory();
		fillTablePositionen();
		getSelectedCat();
	}

	@FXML
	public void btnDeleteSelectedPositionAction(ActionEvent event)
	{
		currentJob.deletePosName(currentOrderPosName);
		rootCurrentJob.getChildren().remove(0,
				rootCurrentJob.getChildren().size());

		for (int i = 0; i < currentJob.getCurrentJobPositionen().size(); i++) {
			rootCurrentJob.getChildren().add(new TreeItem<tableDataCurrentOrder>(
					currentJob.getCurrentJobPositionen().get(i)));

		}
		setJobPrizeLabel(currentJob.getJobValue());

		if (rootCurrentJob.getChildren().size() == 0) {
			btnPrintBill.setDisable(true);
			isPrintBtnDisabled = true;
			tapPosEdit.setDisable(false);
			btnDeleteSelectedPosition.setDisable(true);
		}

		if (!currentJob.existsPosName(currentOrderPosName)) {
			btnDeleteSelectedPosition.setDisable(true);
		}
	}

	@FXML
	public void btnPrintBillAction(ActionEvent event)
	{
		btnPrintBill.setDisable(true);
		tapPosEdit.setDisable(false);
		btnDeleteSelectedPosition.setDisable(true);
		isPrintBtnDisabled = true;
		tapJobs.setDisable(false);

		setJobPrizeLabel(0);

		currentJob.setJobtime(
				timedate.getSystemTime() + " " + timedate.getSystemDate());

		rootCurrentJob.getChildren().remove(0,
				rootCurrentJob.getChildren().size());

		dbc.fillJobs_Jobs(currentJob.getJobnumber(), currentJob.getJobtime(),
				currentJob.createPosQuantityDBString(),
				currentJob.createPosNameDBString(),
				currentJob.createPosValueDBString(),
				currentJob.createPosCatDBString(), "verbucht",
				String.valueOf(currentJob.getJobValue()));

		pj.printJob(currentJob.getJobnumber(), xmlc, dbc, pc);

		fillTableJobs();

		currentJob = null;

		createNewJob();

	}

	@FXML
	public void btnReprintJobAction(ActionEvent event)
	{
		System.out.println("reprint job");
		pj.printJob((selectedJobId + 1), xmlc, dbc, pc);
	}

	@FXML
	public void gridButton01Action(ActionEvent event)
	{
		handleGridButtons(1);
	}

	@FXML
	public void gridButton02Action(ActionEvent event)
	{
		handleGridButtons(2);
	}

	@FXML
	public void gridButton03Action(ActionEvent event)
	{
		handleGridButtons(3);
	}

	@FXML
	public void gridButton04Action(ActionEvent event)
	{
		handleGridButtons(4);
	}

	@FXML
	public void gridButton05Action(ActionEvent event)
	{
		handleGridButtons(5);
	}

	@FXML
	public void gridButton06Action(ActionEvent event)
	{
		handleGridButtons(6);
	}

	@FXML
	public void gridButton07Action(ActionEvent event)
	{
		handleGridButtons(7);
	}

	@FXML
	public void gridButton08Action(ActionEvent event)
	{
		handleGridButtons(8);
	}

	@FXML
	public void gridButton09Action(ActionEvent event)
	{
		handleGridButtons(9);
	}

	@FXML
	public void gridButton10Action(ActionEvent event)
	{
		handleGridButtons(10);
	}

	@FXML
	public void gridButton11Action(ActionEvent event)
	{
		handleGridButtons(11);
	}

	@FXML
	public void gridButton12Action(ActionEvent event)
	{
		handleGridButtons(12);
	}

	@FXML
	public void gridButton13Action(ActionEvent event)
	{
		handleGridButtons(13);
	}

	@FXML
	public void gridButton14Action(ActionEvent event)
	{
		handleGridButtons(14);
	}

	@FXML
	public void gridButton15Action(ActionEvent event)
	{
		handleGridButtons(15);
	}

	@FXML
	public void gridButton16Action(ActionEvent event)
	{
		handleGridButtons(16);
	}

	@FXML
	public void gridButton17Action(ActionEvent event)
	{
		handleGridButtons(17);
	}

	@FXML
	public void gridButton18Action(ActionEvent event)
	{
		handleGridButtons(18);
	}

	@FXML
	public void gridButton19Action(ActionEvent event)
	{
		handleGridButtons(19);
	}

	@FXML
	public void gridButton20Action(ActionEvent event)
	{
		handleGridButtons(20);
	}

	@FXML
	public void gridButton21Action(ActionEvent event)
	{
		handleGridButtons(21);
	}

	@FXML
	public void gridButton22Action(ActionEvent event)
	{
		handleGridButtons(22);
	}

	@FXML
	public void gridButton23Action(ActionEvent event)
	{
		handleGridButtons(23);
	}

	@FXML
	public void gridButton24Action(ActionEvent event)
	{
		handleGridButtons(24);
	}

	@FXML
	public void gridButton25Action(ActionEvent event)
	{
		handleGridButtons(25);
	}

	@FXML
	public void fillTablePositionen()
	{ // loads the table in the TreeTableView

		rootPositionen.getChildren().remove(0,
				rootPositionen.getChildren().size());

		for (int i = 0; i < dbc.ladeTabellePositionen().size(); i++) {

			float fValue = Float
					.valueOf(dbc.ladeTabellePositionen().get(i).getValue());

			String strValue = String.format("%.02f", fValue);

			strValue.replace('.', ',');

			tableDataPositionen helpTableData = new tableDataPositionen(
					dbc.ladeTabellePositionen().get(i).getID(),
					dbc.ladeTabellePositionen().get(i).getName(), (strValue + "€"),
					dbc.getCategoryNameFromPositionen(i + 1),
					getColorNames(dbc.ladeTabellePositionen().get(i).getColor()));
			rootPositionen.getChildren()
					.add(new TreeItem<tableDataPositionen>(helpTableData));
		}
	}

	public void initUI()
	{
		System.out.println("initUI");
		for (int i = 0; i < 25; i++) {
			getButtonByID(i).setVisible(false);
		}
		tftNewDBName.setText(xmlc.getDatabaseName());
		tftKat05.setDisable(true);
		titlePaneStats.setVisible(false);
		btnPrintBill.setDisable(true);
		btnReprintJob.setDisable(true);
		btnCancelJob.setDisable(true);
		btnDeleteSelectedPosition.setDisable(true);
		tapJobs.setDisable(true);
		btnOpenFolder.setFocusTraversable(false);
		switchSeparate.setFocusTraversable(false);
		ueberbtn.setFocusTraversable(false);
		isPrintBtnDisabled = true;
		btnPrintBill
				.setStyle("-fx-background-color: #2ac8fc; -fx-text-fill: black;");
		btnLock.setFocusTraversable(false);
		initPositionen();
		initCurrentOrderTreeTableView();
		initJobTreeTableView();

	}

	public int getSelectedCat()
	{
		ObservableList<String> cats = FXCollections.observableArrayList();

		for (int i = 1; i < 5; i++) {
			if (dbc.getName_Category(i).equals("Standard")) {
			} else {
				cats.add(dbc.getName_Category(i));
			}
		}

		cats.add(dbc.getName_Category(5));

		catChoise.setItems(cats);
		catChoise.getSelectionModel().selectedIndexProperty()
				.addListener(new ChangeListener<Number>() {
					@Override
					public void changed(ObservableValue<? extends Number> ov,
							Number value, Number new_value)
					{
						try {
							selectedCatName = catChoise.getItems().get((int) new_value)
									.toString();
						} catch (Exception e) {
						}
					}
				});

		for (int i = 1; i < 6; i++) {
			if (dbc.getName_Category(i).equals(selectedCatName)) {
				return i;
			}
		}
		return 5;
	}

	public void fillPrinterSettings()
	{
		System.out.println("Fill printer Settings");

		ObservableList<String> printers = FXCollections
				.observableArrayList(pc.getAvailablePrinters());
		printers.add("Drucker auswählen");
		printerChoise.setItems(printers);

		boolean printerFound = false;
		int index = 0;
		for (String currentPrinter : printers) {
			if (xmlc.getPrintername().equals(currentPrinter)) {
				// saved printer is available
				printerFound = true;
				// select saved printer in UI
				printerChoise.getSelectionModel().select(index);
			}
			index++;
		}

		if (!printerFound) {
			// saved printer is not available
			printerChoise.getSelectionModel().select(printers.size() - 1);
			System.out.println("no printer found");
		}

		SpinnerValueFactory<Integer> valueFactoryLines = //
				new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 99,
						xmlc.getLinebreak());

		linesSpinner.setValueFactory(valueFactoryLines);

		SpinnerValueFactory<Integer> valueFactoryHeader = //
				new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 99,
						xmlc.getOffsetHeader());

		offsetHeaderSpinner.setValueFactory(valueFactoryHeader);

		SpinnerValueFactory<Integer> valueFactoryFooter = //
				new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 99,
						xmlc.getOffsetFooter());

		offsetFooterSpinner.setValueFactory(valueFactoryFooter);

		tftheader.setText(xmlc.getHeader());

		tftfooter.setText(xmlc.getFooter());

		switchSeparate.setSelected(xmlc.getCategorySplitted());

	}

	public void fillCategory()
	{
		tftKat01.setText(dbc.getName_Category(1));
		tftKat02.setText(dbc.getName_Category(2));
		tftKat03.setText(dbc.getName_Category(3));
		tftKat04.setText(dbc.getName_Category(4));
		tftKat05.setText(dbc.getName_Category(5));
	}

	private void initPositionen()
	{
		entryTreeTable.setRoot(rootPositionen);
		entryTreeTable.setShowRoot(false);
		entryTreeTable.setEditable(false);

		columnPosnumber
				.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");
		columnPositionsEdit
				.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");
		columnPrize.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");
		columnCat.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");
		columnColor.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");

		ObservableList<String> color = FXCollections.observableArrayList("Rot",
				"Orange", "Braun", "Weiß", "Gelb", "Gr\u00fcn", "Blau", "Indigo");
		colorChoise.setItems(color);
		colorChoise.getSelectionModel().selectedIndexProperty()
				.addListener(new ChangeListener<Number>() {
					@Override
					public void changed(ObservableValue<? extends Number> ov,
							Number value, Number new_value)
					{
						selectedColorName = colorChoise.getItems()
								.get((int) new_value).toString();
					}
				});

		columnPosnumber.setCellValueFactory(
				cellData -> cellData.getValue().getValue().idProperty().asObject());

		columnPositionsEdit.setCellValueFactory(
				cellData -> cellData.getValue().getValue().nameProperty());

		columnPrize.setCellValueFactory(
				cellData -> cellData.getValue().getValue().valueProperty());

		columnCat.setCellValueFactory(
				cellData -> cellData.getValue().getValue().catProperty());

		columnColor.setCellValueFactory(
				cellData -> cellData.getValue().getValue().colorProperty());

		entryTreeTable.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Object>() {
					@Override
					public void changed(ObservableValue<?> observable, Object oldVal,
							Object newVal)
					{
						int selected = entryTreeTable.getSelectionModel()
								.getSelectedIndex(); // get selected item

						try {
							idPositionen = columnPosnumber.getCellData(selected);
							tftNewPosition
									.setText(dbc.getName_Positionen(idPositionen));
							tftNewValue.setText(dbc.getValue_Positionen(idPositionen));
							colorChoise.getSelectionModel().select(
									getColorID(dbc.getColor_Positionen(idPositionen)));

							catChoise.getSelectionModel().select(
									dbc.getCategoryNameFromPositionen(idPositionen));

						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
					}
				});

		tftNewValue.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue)
			{
				if (!newValue.matches("\\d{0,4}([\\.]\\d{0,2})?")) {
					tftNewValue.setText(oldValue);
				}
			}
		});

		final Tooltip tooltipName = new Tooltip();
		tooltipName.setText("Name der neuen Position");
		tooltipName.setStyle("-fx-font: normal bold 20 Cantarell; "
				+ "-fx-base: #AE3522; " + "-fx-text-fill: orange;");
		tftNewPosition.setTooltip(tooltipName);

		final Tooltip tooltipValue = new Tooltip();
		tooltipValue
				.setText("Preis der neuen Position.\nPunkt als Trennzeichen!");
		tooltipValue.setStyle("-fx-font: normal bold 20 Cantarell; "
				+ "-fx-base: #AE3522; " + "-fx-text-fill: orange;");
		tftNewValue.setTooltip(tooltipValue);
		labelNewValue.setTooltip(tooltipValue);

	}

	public void setMain(Main main, DBController dbc, XMLController xmlc,
			PrinterController pc)
	{
		this.main = main;
		this.dbc = dbc;
		this.xmlc = xmlc;
		this.pc = pc;
	}

	public void setDBLabel() throws Exception
	{
		if (xmlc.loadSettings() == true) {
			labelDBStatus.setText(
					"Geladene Datenbank: " + xmlc.getDatabaseName() + ".db");
			btnCreateNewDatabase.setDisable(true);
			tftNewDBName.setDisable(true);
			labelDBName.setTooltip(new Tooltip(
					"Um eine neue Datenbank zu erzeugen muss die vorherige config.xml und "
							+ xmlc.getDatabaseName()
							+ ".db gelöscht werden! Anwendung danach neustarten!"));
			labelDBStatus.setTooltip(new Tooltip(
					"Um eine neue Datenbank zu erzeugen muss die vorherige config.xml und "
							+ xmlc.getDatabaseName()
							+ ".db gelöscht werden! Anwendung danach neustarten!"));
		} else {
			labelDBStatus.setText("Keine Datenbank gefunden!");
		}
	}

	private String getColorCodes(String pColorName)
	{
		switch (pColorName) {
		case "Rot":
			return "#ad0000";
		case "Orange":
			return "#FF4500";
		case "Braun":
			return "#8B4513";
		case "Weiß":
			return "#FAF0E6";
		case "Gelb":
			return "#FFD700";
		case "Gr\u00fcn":
			return "#556B2F";
		case "Blau":
			return "#00BFFF";
		case "Indigo":
			return "#4B0082";

		default:
			return "#FFFFFF";
		}
	}

	private String getColorNames(String pColorCode)
	{
		switch (pColorCode) {
		case "#ad0000":
			return "Rot";
		case "#FF4500":
			return "Orange";
		case "#8B4513":
			return "Braun";
		case "#FAF0E6":
			return "Weiß";
		case "#FFD700":
			return "Gelb";
		case "#556B2F":
			return "Gr\u00fcn";
		case "#00BFFF":
			return "Blau";
		case "#4B0082":
			return "Indigo";

		default:
			return "Farbe";
		}
	}

	private Integer getColorID(String pColorCode)
	{
		switch (pColorCode) {
		case "#ad0000":
			return 0;
		case "#FF4500":
			return 1;
		case "#8B4513":
			return 2;
		case "#FAF0E6":
			return 3;
		case "#FFD700":
			return 4;
		case "#556B2F":
			return 5;
		case "#00BFFF":
			return 6;
		case "#4B0082":
			return 7;

		default:
			return 0;
		}
	}

	public void blockUI(boolean pState)
	{
		btnCalcStats.setDisable(pState);
		btnClearEntry.setDisable(pState);

		if ((!isPrintBtnDisabled) && (!pState)) {
			// print was not disabled and will be enabled again
			btnPrintBill.setDisable(pState);
			btnDeleteSelectedPosition.setDisable(pState);
		}

		if (pState) {
			// disable allways
			btnPrintBill.setDisable(pState);
			btnDeleteSelectedPosition.setDisable(pState);
		}

		btnReprintJob.setDisable(pState);
		btnSaveEntry.setDisable(pState);
		btnCancelJob.setDisable(pState);

		for (int i = 0; i < 25; i++) {
			getButtonByID(i).setDisable(pState);
		}

		tftNewPosition.setDisable(pState);
		tftNewValue.setDisable(pState);
		colorChoise.setDisable(pState);
		catChoise.setDisable(pState);

		tableCurrentOrder.setDisable(pState);
		jobsTreeTable.setDisable(pState);
		entryTreeTable.setDisable(pState);

		labelAllPrize.setVisible(!pState);
		labelJobCounter.setVisible(!pState);

		if (pState) {
			titlePaneStats.setVisible(!pState);
		}

		titledPaneEntry.setDisable(pState);
		titlePaneCat.setDisable(pState);
		titlePanePrint.setDisable(pState);

	}

	public void loadGridButtons()
	{
		for (int i = 0; i < 25; i++) {
			getButtonByID(i).setText(dbc.getName_Positionen(i + 1));
			getButtonByID(i).setFocusTraversable(false);
			if ((getColorID(dbc.getColor_Positionen(i + 1)) == 0)
					|| (getColorID(dbc.getColor_Positionen(i + 1)) == 7)) {
				getButtonByID(i).setStyle("-fx-background-color: "
						+ dbc.getColor_Positionen(i + 1) + "; -fx-text-fill: white;");
			} else {
				getButtonByID(i).setStyle("-fx-background-color: "
						+ dbc.getColor_Positionen(i + 1) + "; -fx-text-fill: black;");
			}
		}
		for (int i = 0; i < 25; i++) {
			if (dbc.getName_Positionen(i + 1).equals("Noch frei")) {
				getButtonByID(i).setVisible(false);
			} else {
				getButtonByID(i).setVisible(true);
			}
		}
	}

	public Button getButtonByID(int pID)
	{
		switch (pID) {
		case 0:
			return gridButton01;
		case 1:
			return gridButton02;
		case 2:
			return gridButton03;
		case 3:
			return gridButton04;
		case 4:
			return gridButton05;
		case 5:
			return gridButton06;
		case 6:
			return gridButton07;
		case 7:
			return gridButton08;
		case 8:
			return gridButton09;
		case 9:
			return gridButton10;
		case 10:
			return gridButton11;
		case 11:
			return gridButton12;
		case 12:
			return gridButton13;
		case 13:
			return gridButton14;
		case 14:
			return gridButton15;
		case 15:
			return gridButton16;
		case 16:
			return gridButton17;
		case 17:
			return gridButton18;
		case 18:
			return gridButton19;
		case 19:
			return gridButton20;
		case 20:
			return gridButton21;
		case 21:
			return gridButton22;
		case 22:
			return gridButton23;
		case 23:
			return gridButton24;
		case 24:
			return gridButton25;
		default:
			return gridButton01;
		}
	}

	private void handleGridButtons(int pID)
	{

		if (!getButtonByID(pID - 1).isVisible()) {
			// Button is not visible, no action
			return;
		}

		currentJob.addPosition(dbc.getName_Positionen(pID),
				Float.parseFloat(dbc.getValue_Positionen(pID)),
				dbc.getCategoryNameFromPositionen(pID));

		btnPrintBill.setDisable(false);
		isPrintBtnDisabled = false;
		tapPosEdit.setDisable(true);

		rootCurrentJob.getChildren().remove(0,
				rootCurrentJob.getChildren().size());

		for (int i = 0; i < currentJob.getCurrentJobPositionen().size(); i++) {
			rootCurrentJob.getChildren().add(new TreeItem<tableDataCurrentOrder>(
					currentJob.getCurrentJobPositionen().get(i)));
		}

		setJobPrizeLabel(currentJob.getJobValue());

		btnPrintBill.setFocusTraversable(false);

	}

	private void initCurrentOrderTreeTableView()
	{
		columnQuantity
				.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");
		columnPosition
				.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");

		tableCurrentOrder.setRoot(rootCurrentJob);
		tableCurrentOrder.setShowRoot(false);
		tableCurrentOrder.setEditable(false);

		columnQuantity.setCellValueFactory(cellData -> cellData.getValue()
				.getValue().quantityProperty().asObject());

		columnPosition.setCellValueFactory(
				cellData -> cellData.getValue().getValue().positionProperty());

		tableCurrentOrder.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Object>() {
					@Override
					public void changed(ObservableValue<?> observable, Object oldVal,
							Object newVal)
					{
						int selected = tableCurrentOrder.getSelectionModel()
								.getSelectedIndex(); // get selected item

						if (columnPosition.getCellData(selected) != null) {
							currentOrderPosName = columnPosition.getCellData(selected); // Ausgewählte
							btnDeleteSelectedPosition.setDisable(false);
						}
					}
				});

	}

	private void initJobTreeTableView()
	{
		columnJobNumber
				.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");

		columnTime.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");

		columnPositions
				.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");

		columnState.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");

		columnJobValue
				.setStyle("-fx-alignment: CENTER; -fx-font: 17px Cantarell;");

		jobsTreeTable.setRoot(rootJobs);
		jobsTreeTable.setShowRoot(false);
		jobsTreeTable.setEditable(false);

		columnJobNumber.setCellValueFactory(cellData -> cellData.getValue()
				.getValue().numberProperty().asObject());

		columnTime.setCellValueFactory(
				cellData -> cellData.getValue().getValue().timeProperty());

		columnPositions.setCellValueFactory(
				cellData -> cellData.getValue().getValue().positionProperty());

		columnState.setCellValueFactory(
				cellData -> cellData.getValue().getValue().statusProperty());

		columnJobValue.setCellValueFactory(
				cellData -> cellData.getValue().getValue().valueProperty());

		jobsTreeTable.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Object>() {
					@Override
					public void changed(ObservableValue<?> observable, Object oldVal,
							Object newVal)
					{
						selectedJobId = jobsTreeTable.getSelectionModel()
								.getSelectedIndex(); // get selected item

						if (dbc.getState_Job(selectedJobId + 1).equals("storniert")) {
							btnCancelJob.setDisable(true);
							btnReprintJob.setDisable(true);
						} else {
							btnCancelJob.setDisable(false);
							btnReprintJob.setDisable(false);
						}
					}
				});
	}

	@FXML
	public void fillTableJobs()
	{ // loads the table in the TreeTableView
		rootJobs.getChildren().remove(0, rootJobs.getChildren().size());

		for (int i = 0; i < dbc.loadTableJobs_Job().size(); i++) {

			float fValue = Float
					.valueOf(dbc.loadTableJobs_Job().get(i).getValue());

			String strValue = String.format("%.02f", fValue);

			strValue.replace('.', ',');

			strValue = (strValue + "€");

			String strJobDescription = createJobDescription(
					dbc.loadTableJobs_Job().get(i).getPosition(),
					dbc.getQuantity_Job(dbc.loadTableJobs_Job().get(i).getNumber()));

			tableDataJob tmp = new tableDataJob(
					dbc.loadTableJobs_Job().get(i).getNumber(),
					dbc.loadTableJobs_Job().get(i).getTime(), strJobDescription,
					dbc.loadTableJobs_Job().get(i).getStatus(), strValue);

			rootJobs.getChildren().add(new TreeItem<tableDataJob>(tmp));
		}

		if (!dbc.getJobCount().equals("0")) {
			tapJobs.setDisable(false);
		}

	}

	public void createNewJob()
	{
		currentJob = new Job(dbc.getLatestJobNumber_Job() + 1);
		labelJobCounter.setText("Auftragsnummer: "
				+ String.valueOf(dbc.getLatestJobNumber_Job() + 1));
	}

	private static String createJobDescription(String pNames, String pQuantities)
	{
		String line = pQuantities;
		int size = line.length() - line.replace(";", "").length() + 1;

		String[] namesArray = new String[size];
		String[] quantitiesArray = new String[size];

		namesArray = pNames.split(";");
		quantitiesArray = pQuantities.split(";");

		String tmp = quantitiesArray[0] + "x " + namesArray[0];

		for (int i = 1; i < namesArray.length; i++) {
			tmp = tmp + ", " + quantitiesArray[i] + "x " + namesArray[i];
		}
		return tmp;
	}

	private void setJobPrizeLabel(float pPrize)
	{
		String str = String.format("%.02f", pPrize);
		str.replace('.', ',');
		labelAllPrize.setText(str + "€");
	}

	private void setJobStatLabel(int pCounter, float pAvgValue, float pAllValue)
	{
		labelJobCount.setText("Anzahl Aufträge: " + pCounter);

		String str = String.format("%.02f", pAvgValue);

		str.replace('.', ',');

		labelAvgJob.setText("Durchschnittlicher Auftragswert: " + str + "€");

		str = String.format("%.02f", pAllValue);

		str.replace('.', ',');

		labelAllValue.setText("Gesamt: " + str + "€");

		titlePaneStats.setText("Statistik - " + timedate.getSystemTime() + " "
				+ timedate.getSystemDate());
	}

	public void updateTimeLabel()
	{
		labelTime.setText("Uhrzeit: " + timedate.getSystemTime());

	}

	public void blockUnlock()
	{
		btnLock.setDisable(true);
	}

	public int getActiveTab()
	{

		if (tapNewJob.isSelected()) {
			return 0;
		}

		if (tapJobs.isSelected()) {
			return 1;
		}

		if (tapPosEdit.isSelected()) {
			return 2;
		}

		if (tapSettings.isSelected()) {
			return 3;
		}

		return 404;
	}

}
