package com.jFxKasse.controller;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class KeyController
{
	private MainWindowController mwc;

	public KeyController(Scene scene, MainWindowController mwc)
	{
		this.mwc = mwc;

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent keyEvent)
			{
				switch (mwc.getActiveTab()) {
				case 0:
					handleTabNewJob(keyEvent);
					break;

				case 1:
					handleTabJobs(keyEvent);
					break;

				case 2:
					handleTabPosEdit(keyEvent);
					break;

				case 3:
					handleTabSettings(keyEvent);
					break;
				default:

				}

			}
		});

	}

	private void handleTabNewJob(KeyEvent key)
	{
		if ((key.getCode() == KeyCode.ENTER)
				&& (!mwc.btnPrintBill.isDisabled())) {
			mwc.btnPrintBillAction(null);
		}

		if ((key.getCode() == KeyCode.ESCAPE) && (!mwc.btnLock.isDisabled())) {
			mwc.btnLockAction(null);
		}

		if ((key.getCode() == KeyCode.DELETE)
				&& (!mwc.btnDeleteSelectedPosition.isDisabled())) {
			mwc.btnDeleteSelectedPositionAction(null);
		}

		handelGridButtons(key);

	}

	private void handleTabJobs(KeyEvent key)
	{
		if ((key.getCode() == KeyCode.ENTER)
				&& (!mwc.btnReprintJob.isDisabled())) {
			mwc.btnReprintJobAction(null);
		}

		if ((key.getCode() == KeyCode.DELETE)
				&& (!mwc.btnCancelJob.isDisabled())) {
			mwc.btnCancelJobAction(null);
		}

		if ((key.getCode() == KeyCode.S) && (!mwc.btnCalcStats.isDisabled())) {
			mwc.btnCalcStatsAction(null);
		}
	}

	private void handleTabPosEdit(KeyEvent key)
	{
		if ((key.getCode() == KeyCode.ENTER)
				&& (!mwc.btnSaveEntry.isDisabled())) {
			mwc.btnSaveEntryAction(null);
		}

		if ((key.getCode() == KeyCode.DELETE)
				&& (!mwc.btnClearEntry.isDisabled())) {
			mwc.btnClearEntryAction(null);
		}
	}

	private void handleTabSettings(KeyEvent key)
	{
		if ((key.getCode() == KeyCode.ENTER)
				&& (!mwc.btnSavePrinter.isDisabled())) {
			mwc.btnSavePrinterAction(null);
		}

		if ((key.getCode() == KeyCode.ENTER) && (!mwc.btnSaveCat.isDisabled())) {
			mwc.btnSaveCatAction(null);
		}

		if ((key.getCode() == KeyCode.ENTER)
				&& (!mwc.btnCreateNewDatabase.isDisabled())) {
			try {
				mwc.btnCreateNewDatabaseAction(null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	void handelGridButtons(KeyEvent key)
	{

		switch (key.getCode()) {
		case Q:
			mwc.gridButton01Action(null);
			break;

		case W:
			mwc.gridButton02Action(null);
			break;

		case E:
			mwc.gridButton03Action(null);
			break;

		case R:
			mwc.gridButton04Action(null);
			break;

		case T:
			mwc.gridButton05Action(null);
			break;

		case Z:
			mwc.gridButton06Action(null);
			break;

		case U:
			mwc.gridButton07Action(null);
			break;

		case I:
			mwc.gridButton08Action(null);
			break;

		case O:
			mwc.gridButton09Action(null);
			break;

		case P:
			mwc.gridButton10Action(null);
			break;

		case A:
			mwc.gridButton11Action(null);
			break;

		case S:
			mwc.gridButton12Action(null);
			break;

		case D:
			mwc.gridButton13Action(null);
			break;

		case F:
			mwc.gridButton14Action(null);
			break;

		case G:
			mwc.gridButton15Action(null);
			break;

		case H:
			mwc.gridButton16Action(null);
			break;

		case J:
			mwc.gridButton17Action(null);
			break;

		case K:
			mwc.gridButton18Action(null);
			break;

		case L:
			mwc.gridButton19Action(null);
			break;

		case Y:
			mwc.gridButton20Action(null);
			break;

		case X:
			mwc.gridButton21Action(null);
			break;

		case C:
			mwc.gridButton22Action(null);
			break;

		case V:
			mwc.gridButton23Action(null);
			break;

		case B:
			mwc.gridButton24Action(null);
			break;

		case N:
			mwc.gridButton25Action(null);
			break;

		default:
			break;

		}

	}

}
