package com.jFxKasse.datatypes;

public abstract class PrintData
{

	protected int headerSpace;

	protected int footerSpace;

	protected int lineBreak;

	protected String header;

	protected String footer;

	protected String positionsQuantity;

	protected String positionsName;

	protected String positionsValue;

	protected String positionsCategory;

	protected String jobID;

	protected String timeAndDateOrder;

	protected String timeAndDatePrint;

	protected String jobValue;

	/**
	 * Constructor with all data that is not in the DB
	 * @param lineBreak
	 * @param headerSpace
	 * @param footerSpace
	 * @param timeAndDatePrint
	 * @param header
	 * @param footer
	 */
	public PrintData(int lineBreak, int headerSpace, int footerSpace,
			String timeAndDatePrint, String header, String footer)
	{
		this.lineBreak = lineBreak;
		this.headerSpace = headerSpace;
		this.footerSpace = footerSpace;
		this.timeAndDatePrint = timeAndDatePrint;
		this.header = header;
		this.footer = footer;
	}

	/**
	 * set all Data that is in the DB
	 * @param jobID
	 * @param timeAndDateOrder
	 * @param positionenQuantity
	 * @param positionenName
	 * @param positionenValue
	 * @param positionenCategory
	 * @param jobValue
	 */
	public void setData(String jobID, String timeAndDateOrder,
			String positionsQuantity, String positionsName,
			String positionsValue, String positionsCategory, String jobValue)
	{
		this.jobID = jobID;
		this.timeAndDateOrder = timeAndDateOrder;
		this.positionsQuantity = positionsQuantity;
		this.positionsName = positionsName;
		this.positionsValue = positionsValue;
		this.positionsCategory = positionsCategory;
		this.jobValue = jobValue;
	}

	/**
	 * 
	 * @param data String
	 * @return same String splitted with \n after the max. line lenght
	 */
	protected String breakLines(String data)
	{
		boolean next = false;

		int count = lineBreak;

		if (data.length() > lineBreak) {
			// Needs to be splitted
			next = true;
		} else {
			// No need to be splitted
			return data;
		}

		// first part
		String tmp = data.substring(0, lineBreak);

		while (next) {

			try {
				tmp = tmp + "\n" + data.substring(count, lineBreak + count);
				count = count + lineBreak;
			} catch (Exception e) {
				// data string not long enough
				next = false;
			}
		}
		// add the last part
		return tmp + "\n" + data.substring(count);
	}

	/**
	 * prints a line of '--------'
	 * @return
	 */
	protected String getSeparator()
	{
		String tmp = "-";

		for (int i = 1; i < lineBreak; i++) {
			tmp = tmp + "-";
		}
		return tmp;
	}

	/**
	 * sets a String into the center
	 * @param data
	 * @return the centered String
	 */
	protected String setCenter(String data)
	{
		int dataLenght = data.length();
		int prefix = ((lineBreak - dataLenght) / 2);
		String tmp = " ";

		for (int i = 1; i < prefix; i++) {
			tmp = tmp + " ";
		}
		tmp = tmp + data;
		
		return breakLines(tmp);
	}

	/**
	 * sets a String right-justified after an prefix
	 * @param prefix
	 * @param data
	 * @return the right-justified String
	 */
	protected String setRight(String prefix, String data)
	{

		int prefixLenght = prefix.length();

		int dataLenght = data.length();

		String tmp = prefix;

		int fill = lineBreak - (prefixLenght + dataLenght);

		if (fill < 0) {
			tmp = tmp + "\n";
			fill = lineBreak - dataLenght;
		}

		for (int i = 0; i < fill; i++) {
			tmp = tmp + " ";
		}

		tmp = tmp + data;

		return tmp;
	}

	/**
	 * How the print String or Strings are made
	 */
	abstract protected void generatePrintString();

}
