package com.jFxKasse.datatypes;

public class Category
{
	private String categoryName;

	private String positionsString = "\n";

	public Category(String categoryName)
	{
		this.categoryName = categoryName;
	}

	public String getCategoryName()
	{
		return categoryName;
	}

	public void addPosition(int quantity, String name, String value,
			PrintData pd)
	{
		for (int i = 0; i < quantity; i++) {
			positionsString = positionsString
					+ pd.setRight(pd.breakLines(name), value + " €") + "\n";
		}
	}

	public String getPositionsString()
	{
		return positionsString;
	}
}
