package com.jFxKasse.datatypes;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class tableDataCurrentOrder
{ 

	private final StringProperty position = new SimpleStringProperty();

	private final IntegerProperty quantity = new SimpleIntegerProperty();

	public tableDataCurrentOrder(final String pPosition, final Integer pQuantity)
	{
		this.position.set(pPosition);
		this.quantity.set(pQuantity);
	}

	public StringProperty positionProperty()
	{
		return position;
	}

	public IntegerProperty quantityProperty()
	{
		return quantity;
	}

	public String getPosition()
	{
		return positionProperty().get();
	}

	public Integer getQuantity()
	{
		return quantityProperty().get();
	}

	public final void setPosition(String pPosition)
	{
		positionProperty().set(pPosition);
	}

	public final void setQuantity(int pQuantity)
	{
		quantityProperty().set(pQuantity);
	}
}
