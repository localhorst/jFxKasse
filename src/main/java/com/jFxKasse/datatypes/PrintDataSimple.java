package com.jFxKasse.datatypes;

public class PrintDataSimple extends PrintData
{

	private String printString;

	/**
	 * Constructor with all data that is not in the DB
	 * @param lineBreak
	 * @param headerSpace
	 * @param footerSpace
	 * @param timeAndDatePrint
	 * @param header
	 * @param footer
	 */
	public PrintDataSimple(int lineBreak, int headerSpace, int footerSpace,
			String timeAndDatePrint, String header, String footer)
	{
		super(lineBreak, headerSpace, footerSpace, timeAndDatePrint, header,
				footer);
	}

	/**
	 * Generates the String
	 * @return the final Print String
	 */
	public String getPrintString()
	{
		generatePrintString();
		return this.printString;
	}

	@Override
	protected void generatePrintString()
	{
		/* Header */
		String header = " ";
		for (int i = 1; i < headerSpace; i++) {
			header = header + "\n";
		}

		// This is the final header
		header = header + setCenter(this.header);

		/* Info */

		String info = setRight("Bestellung: ", timeAndDateOrder) + "\n"
				+ setRight("Druck: ", timeAndDatePrint) + "\n"
				+ setRight("Bestellnummer: ", jobID);

		/* Positionen */

		String positionen = "\n";

		int posCount = positionsQuantity.length()
				- positionsQuantity.replace(";", "").length() + 1;

		String[] positionQuantity = positionsQuantity.split(";");

		String[] positionName = positionsName.split(";");

		String[] positionValue = positionsValue.split(";");

		for (int i = 0; i < posCount; i++) { // All different posNames
			int quantity = Integer.parseInt(positionQuantity[i]);
			for (int j = 0; j < quantity; j++) { // quantities
				positionen = positionen + setRight(breakLines(positionName[i]),
						positionValue[i] + " €") + "\n";
			}
		}

		/* Price */

		String price = setRight("Gesamt: ", (jobValue + " €"));

		/* Footer */

		String footer = setCenter(this.footer);

		for (int i = 1; i < footerSpace; i++) {
			footer = footer + "\n";
		}
		footer = footer + "_";

		/* Build final Print String */

		printString = header + "\n" + getSeparator() + "\n" + info + "\n"
				+ getSeparator() + "\n" + positionen + "\n" + getSeparator() + "\n"
				+ price + "\n" + getSeparator() + "\n" + footer;
	}

}
