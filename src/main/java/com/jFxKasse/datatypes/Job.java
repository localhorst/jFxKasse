package com.jFxKasse.datatypes;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Job
{
	private int jobnumber;

	private float jobvalue;

	private String jobtime;

	private ArrayList<Integer> positionenQuantity;

	private ArrayList<String> positionenName;

	private ArrayList<Float> positionenValue;

	private ArrayList<String> positionenCat;

	public Job(int pJobnumber)
	{
		this.jobnumber = pJobnumber;
		positionenQuantity = new ArrayList<Integer>();
		positionenName = new ArrayList<String>();
		positionenValue = new ArrayList<Float>();
		positionenCat = new ArrayList<String>();

	}

	public void setJobtime(String jobtime)
	{
		this.jobtime = jobtime;
	}

	public int getJobnumber()
	{
		return this.jobnumber;
	}

	public String getJobtime()
	{
		return this.jobtime;
	}

	public float getJobValue()
	{

		calcJobValue();

		return this.jobvalue;
	}

	public void addPosition(String pPositionenName, float pPositionenValue,
			String pPositionenCat)
	{
		for (int i = 0; i < positionenName.size(); i++) {
			if (positionenName.get(i).equals(pPositionenName)) {
				// Item is already in list, increase quantity
				positionenQuantity.set(i, positionenQuantity.get(i) + 1);
				return;
			}
		}

		positionenName.add(pPositionenName);
		positionenValue.add(pPositionenValue);
		positionenCat.add(pPositionenCat);
		positionenQuantity.add(1);

		calcJobValue();
	}

	public void printJobOnConsole()
	{
		System.out.println("---------------------------------------------");
		System.out.println("JobNummer: " + jobnumber);
		System.out.println("---------------------------------------------");

		for (int i = 0; i < positionenName.size(); i++) {

			System.out.println(
					positionenQuantity.get(i) + " " + positionenName.get(i) + " "
							+ positionenValue.get(i) + " " + positionenCat.get(i));
		}

		System.out.println("---------------------------------------------");

	}

	public ArrayList<tableDataCurrentOrder> getCurrentJobPositionen()
	{
		ArrayList<tableDataCurrentOrder> jobitems = new ArrayList<tableDataCurrentOrder>();
		
		for (int i = 0; i < positionenName.size(); i++) {

			tableDataCurrentOrder tmp = new tableDataCurrentOrder(
					positionenName.get(i), positionenQuantity.get(i));

			jobitems.add(tmp);
		}
		return jobitems;
	}

	private void calcJobValue()
	{
		jobvalue = 0;

		for (int i = 0; i < positionenValue.size(); i++) {
			jobvalue = jobvalue
					+ (positionenQuantity.get(i) * positionenValue.get(i));
		}
		//Round to two decimals
		jobvalue =  BigDecimal.valueOf(jobvalue).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
	}

	public String createPosQuantityDBString()
	{
		String tmp = String.valueOf(positionenQuantity.get(0));
		for (int i = 1; i < positionenName.size(); i++) {

			tmp = tmp + ";" + positionenQuantity.get(i);
		}
		return tmp;
	}

	public String createPosNameDBString()
	{
		String tmp = positionenName.get(0);
		for (int i = 1; i < positionenName.size(); i++) {

			tmp = tmp + ";" + positionenName.get(i);
		}
		return tmp;
	}

	public String createPosValueDBString()
	{
		String tmp = String.valueOf(positionenValue.get(0));
		for (int i = 1; i < positionenName.size(); i++) {

			tmp = tmp + ";" + positionenValue.get(i);
		}
		return tmp;
	}

	public String createPosCatDBString()
	{
		String tmp = positionenCat.get(0);
		for (int i = 1; i < positionenName.size(); i++) {

			tmp = tmp + ";" + positionenCat.get(i);
		}
		return tmp;
	}

	public void deletePosName(String pPosName)
	{

		for (int i = 0; i < positionenName.size(); i++) {

			if (positionenName.get(i).equals(pPosName)) {

				if (positionenQuantity.get(i) > 1) {
					positionenQuantity.set(i, positionenQuantity.get(i) - 1);
				} else {

					positionenQuantity.remove(i);
					positionenName.remove(i);
					positionenValue.remove(i);
					positionenCat.remove(i);

				}

			}

		}

	}

	public boolean existsPosName(String pPosName)
	{
		for (int i = 0; i < positionenName.size(); i++) {
			if (positionenName.get(i).equals(pPosName)) {
				return true;
			}
		}

		return false;
	}

}
