package com.jFxKasse.datatypes;

import java.util.ArrayList;

public class PrintDataSplitted extends PrintData
{
	private ArrayList<String> printString = new ArrayList<String>();

	private ArrayList<Category> categories = new ArrayList<Category>();

	private int categoryCount = 0;

	/**
	 * Constructor with all data that is not in the DB
	 * @param lineBreak
	 * @param headerSpace
	 * @param footerSpace
	 * @param timeAndDatePrint
	 * @param header
	 * @param footer
	 */
	public PrintDataSplitted(int lineBreak, int headerSpace, int footerSpace,
			String timeAndDatePrint, String header, String footer)
	{
		super(lineBreak, headerSpace, footerSpace, timeAndDatePrint, header,
				footer);
	}

	/**
	 * Generates the String
	 * @return the final Array with the Print Strings
	 */
	public ArrayList<String> getPrintStrings()
	{
		generatePrintString();
		return printString;
	}

	@Override
	protected void generatePrintString()
	{

		String firstBill;

		/* Header */
		String header = "-";
		for (int i = 1; i < headerSpace; i++) {
			header = header + "\n";
		}

		// This is the final header
		header = header + setCenter(this.header);

		/* Info */

		String info = setRight("Bestellung: ", timeAndDateOrder) + "\n"
				+ setRight("Druck: ", timeAndDatePrint) + "\n"
				+ setRight("Bestellnummer: ", jobID);

		/* Splitted Bills */

		/* Price */

		String price = setRight("Gesamt: ", (jobValue + " €"));

		/* Footer */

		String footer = setCenter(this.footer);

		for (int i = 1; i < footerSpace; i++) {
			footer = footer + "\n";
		}
		footer = footer + "_";

		/* Build first Print String */

		firstBill = header + "\n" + getSeparator() + "\n" + info + "\n"
				+ getSeparator() + "\n" + setCenter("Bon wurde aufgeteilt") + "\n"
				+ getSeparator() + "\n" + price + "\n" + getSeparator() + "\n"
				+ footer;

		printString.add(firstBill);

		/* first bill ends here */

		/* Categories in extra bills */

		String positions = null;

		int posCount = positionsQuantity.length()
				- positionsQuantity.replace(";", "").length() + 1;

		String[] positionQuantity = positionsQuantity.split(";");
		String[] positionName = positionsName.split(";");
		String[] positionValue = positionsValue.split(";");
		String[] positionCategory = positionsCategory.split(";");

		for (int i = 0; i < posCount; i++) { // All different posNames
			int quantity = Integer.parseInt(positionQuantity[i]);
			insertToCategory(quantity, positionName[i], positionValue[i],
					positionCategory[i]);
		}

		// loops through all categories
		for (int i = 0; i < categories.size(); i++) {

			String thisBill;

			/* Header */
			header = " ";
			for (int o = 1; o < headerSpace; o++) {
				header = header + "\n";
			}

			// This is the final header
			header = header + setCenter(this.header);

			/* Info */

			info = setRight("Bestellung: ", timeAndDateOrder) + "\n"
					+ setRight("Druck: ", timeAndDatePrint) + "\n"
					+ setRight("Bestellnummer: ", jobID);

			/* Positions */

			positions = categories.get(i).getPositionsString();

			/* Footer */

			footer = "\n";

			for (int o = 2; o < footerSpace; o++) {
				footer = footer + "\n";
			}
			footer = footer + "_";

			thisBill = header + "\n" + getSeparator() + "\n" + info + "\n"
					+ getSeparator() + "\n"
					+ setCenter(categories.get(i).getCategoryName()) + "\n"
					+ getSeparator() + positions + "\n" + getSeparator() + "\n"
					+ footer;

			printString.add(thisBill);

		}

	}

	private void insertToCategory(int quantity, String name, String value,
			String category)
	{
		boolean createNewCategorie = true;
		for (int i = 0; i < categoryCount; i++) {
			if (category.equals(categories.get(i).getCategoryName())) {
				categories.get(i).addPosition(quantity, name, value, this);
				createNewCategorie = false;
			}
		}

		if (createNewCategorie) {
			// position has a new category
			categories.add(new Category(category));
			categories.get(categoryCount).addPosition(quantity, name, value, this);
			categoryCount++;
		}

	}

}
