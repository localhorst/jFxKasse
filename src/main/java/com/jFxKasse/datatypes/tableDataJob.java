package com.jFxKasse.datatypes;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class tableDataJob
{

	private final IntegerProperty number = new SimpleIntegerProperty();

	private final StringProperty time = new SimpleStringProperty();

	private final StringProperty positionen = new SimpleStringProperty();

	private final StringProperty status = new SimpleStringProperty();

	private final StringProperty value = new SimpleStringProperty();

	public tableDataJob(final Integer pNumber, final String pTime,
			final String pPositionen, final String pStatus, final String pValue)
	{
		this.number.set(pNumber);
		this.time.set(pTime);
		this.positionen.set(pPositionen);
		this.status.set(pStatus);
		this.value.set(pValue);

	}

	public IntegerProperty numberProperty()
	{
		return number;
	}

	public StringProperty timeProperty()
	{
		return time;
	}

	public StringProperty positionProperty()
	{
		return positionen;
	}

	public StringProperty statusProperty()
	{
		return status;
	}

	public StringProperty valueProperty()
	{
		return value;
	}

	public Integer getNumber()
	{
		return numberProperty().get();
	}

	public String getTime()
	{
		return timeProperty().get();
	}

	public String getPosition()
	{
		return positionProperty().get();
	}

	public String getStatus()
	{
		return statusProperty().get();
	}

	public String getValue()
	{
		return valueProperty().get();
	}

	public final void setNumber(int pNumber)
	{
		numberProperty().set(pNumber);
	}

	public final void setTime(String pTime)
	{
		timeProperty().set(pTime);
	}

	public final void setPosition(String pPosition)
	{
		positionProperty().set(pPosition);
	}

	public final void setStatus(String pStatus)
	{
		statusProperty().set(pStatus);
	}

	public final void setValue(String pValue)
	{
		valueProperty().set(pValue);
	}

}
