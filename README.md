# jFxKasse
easy payment system for small to middel sized events with a sales slip printer

Simples Kassensystem für kleine bis mittlere Veranstaltungen mit Bon-Drucker

## Video Tutorials (German | Deutsch)
[YouTube Video: jFxKasse - Kassensystem - Kurzanleitung](https://www.youtube.com/watch?v=DV9DDESw40I)

[YouTube Video: jFxKasse - Kassensystem - Installieren](https://www.youtube.com/watch?v=IY1bqRjwh0Q)

## Screenshots

### Main View | Hauptansicht
![](https://git.mosad.xyz/localhorst/jFxKasse/raw/branch/master/screenshots/newjob)

### Jobs | Auftäge
![](https://git.mosad.xyz/localhorst/jFxKasse/raw/branch/master/screenshots/jobs)

### Positions | Positionen
![](https://git.mosad.xyz/localhorst/jFxKasse/raw/branch/master/screenshots/positions)

### Settings | Einstellungen
![](https://git.mosad.xyz/localhorst/jFxKasse/raw/branch/master/screenshots/settings)


## Requirements | Anforderungen

### Software

* Java JRE 11
* Display/Bildschirm > 1366px X 768px
* Windoofs, Mac, GNU/Linux (openSuse tested)

### Hardware

I used this printer: [Epson TM T20II](https://www.epson.de/products/sd/pos-printer/epson-tm-t20ii)

Other sales slip printer are possible. 